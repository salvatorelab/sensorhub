import Adafruit_DHT as dht
from time import sleep
import json
import urllib2
import time

DHT = 4
ENDPOINT = 'http://sensorhub:8000/sensors/api/data/interior'


def send(temperature, humidity):
    unix_timestamp = int(time.time() * 1000)
    data = {
        'date': unix_timestamp,
        'temperature': temperature,
        'humidity': humidity
    }

    req = urllib2.Request(ENDPOINT)
    req.add_header('Content-Type', 'application/json')

    response = urllib2.urlopen(req, json.dumps(data))


try:
    h, t = dht.read_retry(dht.DHT22, DHT)
    print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(t, h))
    send(round(t, 1), round(h, 1))
except:
    print('Error sending sensor data')

