const OPTIONS = [
    {text: 'Last 1h', value: 1},
    {text: 'Last 8h', value: 8},
    {text: 'Last 24h', value: 24, default: true},
    {text: 'Last 2 days', value: 48},
    {text: 'Last 7 days', value: 24*7},
    {text: 'Last 30 days', value: 24*30},
];
const URL_RANGE_PARAM = 'range';

class DatePicker {

    constructor(containerSelector, onChange) {
        this.container = $(containerSelector);
        this.onChange = onChange;

        this.init();
    }

    init() {
        this.createSelectElement();
        this.addOnChangeListener();
        this.container.find('#datepicker-range').change();
    }

    createSelectElement() {
        let select = document.createElement('select');
        select.id = 'datepicker-range';
        select.classList.add('form-select', 'dark');
        let url = new URL(window.location);
        let rangeParam = url.searchParams.get(URL_RANGE_PARAM);
        for (var i = 0; i < OPTIONS.length; i++) {
            var option = document.createElement("option");
            option.value = OPTIONS[i].value;
            option.text = OPTIONS[i].text;

            // range param in URL takes precedence
            if (rangeParam == OPTIONS[i].value) {
                option.selected = true;
            } else if (!rangeParam && OPTIONS[i].default) {
                option.selected = true;
            }
            select.appendChild(option);
        }
        this.container.append(select);
        // make sure the 'selected' option is actually selected (Firefox selects the last one the user picked)
        this.container.find("#datepicker-range option[selected]").prop('selected', true);
    }

    addOnChangeListener() {
        let onChange = this.onChange;
        let datePickChanged = this.datePickChanged;
        this.container.find('#datepicker-range').on('change', (event) => {
            datePickChanged(event, onChange);
        });
    }

    datePickChanged(event, callback) {
        let fromTimestamp = (new Date()).getTime() - event.currentTarget.value * 60 * 60 * 1000;
        let toTimestamp = (new Date()).getTime();
        
        const url = new URL(window.location);
        url.searchParams.set(URL_RANGE_PARAM, event.currentTarget.value);
        window.history.pushState({}, '', url);

        callback({
            from: fromTimestamp,
            to: toTimestamp
        });
    }

}