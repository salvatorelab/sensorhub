from django.db import models


class Sensor(models.Model):
    TEMPERATURE = 'temperature'
    PRESSURE = 'pressure'
    LUMINANCE = 'luminance'
    SENSOR_TYPES = [
        (TEMPERATURE, 'Temperature and Humidity'),
        (PRESSURE, 'Atmospheric pressure'),
        (LUMINANCE, 'Ambient luminance')
    ]
    SENSOR_FORMATS = dict([
        (TEMPERATURE, 'temperature,humidity,date'),
        (PRESSURE, 'pressure,date'),
        (LUMINANCE, 'lux,date')
    ])

    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    type = models.CharField(
        max_length=20,
        choices=SENSOR_TYPES,
        default=TEMPERATURE,
    )
    format = models.CharField(blank=True, max_length=200,
                              help_text="Leave empty to use the default format for the sensor (recommended).")
    creation_date = models.DateTimeField('creation date')

    def save(self, *args, **kwargs):
        if self.format == '':
            self.format = self.SENSOR_FORMATS[self.type]
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

